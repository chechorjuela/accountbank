import {Component, OnInit} from '@angular/core';
import {SigninContract} from '../../../../Infrastructure/Contracts/signin.contract';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../Infrastructure/Services/Auth/Auth.Service';
import {AppSettings} from '../../../../config/app.settings';
import {Router} from '@angular/router';
import {AuthModel} from '../../../../Infrastructure/Contracts/Common/AuthModel';
import {LocalStorageUtilsService} from '../../../../Infrastructure/Helpers/utils/LocalStorageUtils.service';
import {AuthComunicationService} from '../../../../Infrastructure/Custom/AuthComunication.service';
import {MzToastService} from 'ngx-materialize';

@Component({
  selector: 'signin-selector-aap',
  providers: [AuthService, LocalStorageUtilsService],
  templateUrl: '../../../../views/public/root/signin/signin.component.html'
})

export class SigninComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private appSettings: AppSettings,
    private router: Router,
    private localStorageUtils: LocalStorageUtilsService<AuthModel>,
    private authComunnicationCurrentUser: AuthComunicationService,
    private toastService: MzToastService,
    private formBuilder: FormBuilder,
  ) {
    this.codeResponseSuccess = appSettings.statusResponse.ok;
    this.keyStringStore = appSettings.userKeyStore;
    this.buildForm();
  }

  signIn: SigninContract = new SigninContract();
  loading = false;
  codeResponseSuccess: string;
  keyStringStore: string;
  userForm: FormGroup;

  errorMessageResources = {
    password: {
      required: 'Firstname is required',
      minLength: 'Minimum 4 charaters',
      maxLength: 'Maximum 50 charaters'
    },
    username: {
      required: 'username is required'
    }
  };

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuilder.group({
      password: [this.signIn.password, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      username: [this.signIn.username, Validators.compose([
        Validators.required,
      ])]
    });
  }

  clickLogin(e) {
    e.preventDefault();
    if (this.userForm.valid) {
      this.loading = true;
      this.authService.Signin(this.signIn).then(resp => {

        if (resp.Header.status == 'success') {
          if (resp.Data != null) {
            this.loading = false;
            this.localStorageUtils.deleteAuth();
            this.localStorageUtils.setItem(this.keyStringStore, resp.Data);
            this.authComunnicationCurrentUser.announceCurrentUSerAuth(resp.Data);
            this.toastService.show('Logeado', 4000, 'green', () => console.info('mostrar'));

            this.router.navigate(['auth/user']);
          } else {
            this.toastService.show('User not found', 4000, 'orange', () => console.info('mostrar'));
            this.loading = false;
          }

        }
      }).catch(error => {
        this.toastService.show('!ups a error', 4000, 'red', () => console.info('mostrar'));
      });
    }
  }
}
