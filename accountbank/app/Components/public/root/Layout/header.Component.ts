import {Component, OnInit} from '@angular/core';
import {LocalStorageUtilsService} from '../../../../Infrastructure/Helpers/utils/LocalStorageUtils.service';
import {AuthModel} from '../../../../Infrastructure/Contracts/Common/AuthModel';
import {AppSettings} from '../../../../config/app.settings';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AuthComunicationService} from "../../../../Infrastructure/Custom/AuthComunication.service";

@Component({
  selector: 'app-header-selector',
  templateUrl: '../../../../views/public/root/Share/header.component.html'
})

export class HeaderComponent implements OnInit {
  public user = false;
  public localstorageUser;
  subscriptionGetUser: Subscription;

  constructor(
    private appSettings: AppSettings,
    private router: Router,
    private localStorageUtilsService: LocalStorageUtilsService<AuthModel>,
    private communicationGetUser: AuthComunicationService
  ) {

    this.subscriptionGetUser = this.communicationGetUser.currentUserAnnounce$.subscribe(
      client => {
        if (client) {
          this.user = true;
        }
        //this.taskContractInput = task;
      }
    );
    this.localstorageUser = localStorageUtilsService.getObject(this.appSettings.userKeyStore);
    if (this.localstorageUser) {
      this.user = true;
    }
  }


  ngOnInit(): void {

  }

  logout(e) {
    e.preventDefault();
    this.localStorageUtilsService.deleteAuth();
    this.router.navigate(['/signin']);
    this.user = false;
  }
}
