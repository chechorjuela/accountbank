import {Injectable} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Injectable()
export class CustomValidationService {

  // Name validation
  static ageAdult(control: FormControl) {
    if (control.value) {
      const dateBirth = new Date(control.value);
      const dateDiff = Date.now() - dateBirth.getTime();
      const dateDiffTime = Math.abs(new Date(dateDiff).getUTCFullYear() - 1970);

      return dateDiffTime >= 18 ? null : {invalidAge: true};
    } else {
      return null;
    }
  }

  static validatorRangeSalario(control: FormControl) {
    if (control.value) {
      return control.value > 0 ? null : {invalidSalario: true};
    } else {
      return null;
    }
  }

  static validtorYearCompany(control: FormControl) {
    if (control.value) {
      const dateCompany = new Date(control.value);
      const dateDiff = Date.now() - dateCompany.getTime();
      const dateDiffTime = Math.abs(new Date(dateDiff).getUTCFullYear() - 1970);

      return dateDiffTime >= 1 ? null : {invalidAgeCompany: true};
    } else {
      return null;
    }
  }
  static checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPass.value;
    console.info(group);
    return pass === confirmPass ? null : { notSame: true }
  }
}
