<?php


namespace App\Infrastructure\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'firstname' => 'required:min',
                    'lastname' => 'required',
                ];
                break;
        }
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'firstname.required' => 'El :attribute es requerido',
            'firstname.min' => 'El :attribute debe ser :value',
            'lastname.required' => 'El :attribute  es requerido',
            'lastname.min' => 'El :attribute debe contener :value',
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $json = [];
        $json["code"] = 200;
        $json["data"] = $validator->errors();
        throw new HttpResponseException(response()->json($json, 422));
    }
}