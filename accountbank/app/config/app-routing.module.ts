import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PublicRoutes} from "../Components/public/public.routes";
import {AuthRoutes} from "../Components/admin/auth.routes";


export const routes: Routes = [
  ...PublicRoutes,
  ...AuthRoutes
];

export const routing = RouterModule.forRoot(routes);
