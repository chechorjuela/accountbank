import {ModelCreditApproved} from '../../Infrastructure/Contracts/Common/ModelCreditApproved';

export class EnumCreditApproved {
  public arrayCreditApproved: Array<ModelCreditApproved> = new Array<ModelCreditApproved>();
  public arrayCredit = ['no aprovado', '5.000.000', '20.000.000', '50.000.000'];

  constructor() {
    this.arrayCredit.forEach((item, key) => {
      const credit = new ModelCreditApproved();
      credit.creditApproved = item;
      credit.salarioBetween = key;
      this.arrayCreditApproved.push(credit);
    });
  }
}
