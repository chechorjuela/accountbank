import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {routing} from './app-routing.module';
import {AppComponent} from '../Components/public/root/app.component';

/**
 * Declarations materialize
 */
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterializeComponentModule} from './Modules/materialize.module';
import {SignupComponent} from '../Components/public/root/Signup/signup.component';
import {SigninComponent} from '../Components/public/root/Signin/signin.component';
import {HeaderComponent} from '../Components/public/root/Layout/header.Component';
import {UserListComponent} from '../Components/admin/User/UserList.Component';
import {ProfileComponent} from '../Components/admin/Profile/Profile.Component';
import {ModalUserComponent} from '../Components/admin/User/Modal/ModalUser.Component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalDialogModule} from 'ngx-modal-dialog';
import {ClientRepository, IClientRepository} from '../Repository/Interfaces/client.repository';
import {ClientService, IClientService} from '../Infrastructure/Services/Client/client.service';
import {AppSettings} from './app.settings';
import {HttpClientModule} from '@angular/common/http';
import {ModalAproveCreditComponent} from "../Components/admin/User/Modal/ModalAproveCredit.Component";
import {ClientComuncationService} from "../Infrastructure/Custom/ClientComuncationService";
import {LocalStorageUtilsService} from "../Infrastructure/Helpers/utils/LocalStorageUtils.service";
import {AuthService, IAuthService} from "../Infrastructure/Services/Auth/Auth.Service";
import {AuthRepositroy, IAuthRepository} from "../Repository/Interfaces/Auth/auth.repository";
import {AuthGuard} from "../Infrastructure/Helpers/utils/AuthGuard";
import {AuthComunicationService} from "../Infrastructure/Custom/AuthComunication.service";


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    HeaderComponent,
    UserListComponent,
    ProfileComponent,
    ModalUserComponent,
    ModalAproveCreditComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterializeComponentModule.forRoot(),
    ModalDialogModule.forRoot(),
  ],
  entryComponents: [
    ModalUserComponent,
    ModalAproveCreditComponent,
  ],
  providers: [
    AppSettings,
    ClientComuncationService,
    LocalStorageUtilsService,
    AuthComunicationService,
    AuthGuard,
    {provide: IAuthRepository, useClass: AuthRepositroy},
    {provide: IAuthService, useClass: AuthService},
    {provide: IClientRepository, useClass: ClientRepository},
    {provide: IClientService, useClass: ClientService},

  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
