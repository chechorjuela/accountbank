import {Component, OnDestroy, OnInit} from '@angular/core';
import {MzBaseModal, MzToastService} from 'ngx-materialize';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {UserSignUpContract} from '../../../../Infrastructure/Contracts/signup.contract';
import {AuthService} from "../../../../Infrastructure/Services/Auth/Auth.Service";
import {AppSettings} from "../../../../config/app.settings";
import {Router} from "@angular/router";
import {CustomValidationService} from "../../../../Infrastructure/Custom/customvalidator.service";

@Component({
  selector: 'app-singup-selector',
  providers: [AuthService],
  templateUrl: '../../../../views/public/root/signup/singup.component.html'
})

export class SignupComponent implements OnInit, OnDestroy {

  public loading = true;
  signupForm: FormGroup;
  userSingup: UserSignUpContract = new UserSignUpContract();
  public datepickerOptions: Pickadate.DateOptions = {
    clear: 'Clear', // Clear button text
    close: 'Ok',    // Ok button text
    today: 'Today', // Today button text
    closeOnClear: true,
    closeOnSelect: true,
    format: 'yyyy/mm/dd', // Visible date format (defaulted to formatSubmit if provided otherwise 'd mmmm, yyyy')
    formatSubmit: 'yyyy/mm/dd',   // Return value format (used to set/get value)
    onClose: () => console.info('Close has been invoked.'),
    onOpen: () => console.info('Open has been invoked.'),
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 10,    // Creates a dropdown of 10 years to control year,
  };

  ngOnDestroy(): void {
  }

  ngOnInit(): void {

  }

  constructor(
    private formBuilder: FormBuilder,
    private toastService: MzToastService,
    private router: Router,
    private appSettings: AppSettings,
    public authService: AuthService,
  ) {
    this.buildForm();
  }

  buildForm() {
    this.signupForm = this.formBuilder.group({
      username: [this.userSingup.username, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      password: [this.userSingup.password, Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
      ])],
      confirmPass: [this.userSingup.confirmPass, Validators.compose([
        Validators.required,

      ])]
    }, {validators: CustomValidationService.checkPasswords});
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPass.value;
    return pass === confirmPass ? null : {notSame: true}
  }

  public singup(event) {
    this.loading = true;
    if (this.signupForm.valid) {
      this.authService.Signup(this.signupForm.value).then(resp => {
        if (resp.Header.status == 'success' && resp.Data != null) {
          this.toastService.show('User register', 4000, 'green', () => console.info(''));
          this.router.navigate(['signin']);
        }
        this.loading = true;
      }).catch(error => {
        this.loading = true;
        this.toastService.show('!ups un error', 4000, 'red', () => console.info(''));
      });
    } else {
      const getErrors = this.getErrorsForm(this.signupForm.controls);
      this.toastService.show(getErrors, 4000, 'red', () => console.info(''));
      this.loading = true;

    }
  }
  getErrorsForm(controls) {
    let errorString = '';
    Object.keys(controls).forEach(key => {
      const controlError: ValidationErrors = this.signupForm.get(key).errors;
      if (controlError != null) {
        Object.keys(controlError).forEach(keyError => {
          const keyToString = key.toString().toLowerCase();
          errorString += keyToString + (keyError === 'invalidAge' ? ' age not is 18+' : ' is invalid </br>');
        });
      }
    });

    return errorString;
  }
}
