export class AuthModel {
  public id: number;
  public active: number;
  public created_at: Date;
  public updated_at: Date;
  public token: string;
}
