import {Injectable} from "@angular/core";
import {SigninContract} from "../../../Infrastructure/Contracts/signin.contract";
import {UserSignUpContract} from "../../../Infrastructure/Contracts/signup.contract";
import {ResponseapiContract} from "../../../Infrastructure/Contracts/Common/responseapi.contract";
import {AuthModel} from "../../../Infrastructure/Contracts/Common/AuthModel";
import {AppSettings} from "../../../config/app.settings";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {LocalStorageUtilsService} from "../../../Infrastructure/Helpers/utils/LocalStorageUtils.service";

@Injectable()
export abstract class IAuthRepository {
  abstract SignIn(signin: SigninContract): Promise<ResponseapiContract<AuthModel>>;

  abstract SignUp(signup: UserSignUpContract): Promise<ResponseapiContract<AuthModel>>;

  abstract GetUSer(): Promise<ResponseapiContract<AuthModel>>;

  abstract Logout();
}

@Injectable()
export class AuthRepositroy implements IAuthRepository {
  private token: AuthModel;
  private baseUrl = new AppSettings().urlApi;
  private signinUrl = new AppSettings().urls.signInUrl;
  private singUpUrl = new AppSettings().urls.signUpUrl;
  private getUser = new AppSettings().urls.getUser;
  private headersBasic = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': null});

  constructor(
    private http: HttpClient,
    private appSettings: AppSettings,
    private localStorageUtils: LocalStorageUtilsService<AuthModel>,
  ) {
    this.baseUrl = appSettings.urlApi;
    this.token = this.localStorageUtils.getObject(appSettings.userKeyStore);

  }

  Logout() {

  }

  SignIn(signin: SigninContract): Promise<ResponseapiContract<AuthModel>> {
    // @ts-ignore
    return this.http.post<ResponseApiModel<AuthModel>>(this.baseUrl + this.signinUrl, signin)
      .toPromise()
      .then(response => {
        //let responseApi:ResponseApiModel<AuthModel> = new ResponseApiModel<AuthModel>();
        return response;
      })
      .catch(error => {
      });
  }

  SignUp(signup: UserSignUpContract): Promise<ResponseapiContract<AuthModel>> {
    // @ts-ignore
    return this.http.post<ResponseApiModel<UserModel>>(this.baseUrl + this.singUpUrl, signup)
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(error => {
      });
  }

  GetUSer(): Promise<ResponseapiContract<AuthModel>> {
    this.token = this.localStorageUtils.getObject(this.appSettings.userKeyStore);
    let headersoptions = new HttpHeaders().set('Authorization', 'bearer ' + this.token.token);
    // @ts-ignore
    return this.http.get<ResponseApiModel<AuthModel>>(this.baseUrl + this.getUser, {headers: headersoptions})
      .toPromise()
      .then(resp => {
        return resp;
      }).catch(onreject => {
        return onreject;
      })
  }

}
