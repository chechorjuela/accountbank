<?php


namespace App\Domain\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $id;

    protected $firstname;

    protected $lastname;

    protected $type_document;

    protected $number_id;

    protected $client;

    protected $birthdate;

    protected $fillable = [
        'firstname', 'lastname', 'type_document', 'number_id', 'client', 'birthdate'
    ];

    public $timestamps = false;

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

    }

    /**
     * *
     ***/
    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

    }

    /**
     * *
     ***/
    public function getLastname()
    {
        return $this->lastname;
    }

    public function setTypeDocument($type_document)
    {
        $this->type_document = $type_document;
    }

    public function getTypeDocument()
    {
        return $this->type_document;
    }

    public function setBirthDate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    public function getBirthDate()
    {
        return $this->birthdate;
    }

    public function setClient($client)
    {
        $this->client = $client;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setNumberId($number_id)
    {
        $this->number_id = $number_id;
    }

    public function getNumberId()
    {
        return $this->number_id;
    }

    public function serializeData(): array
    {
        $dataArray = [
            'firstname' => $this->getFirstname(),
            'lastname' => $this->getLastname(),
            'type_document' => $this->getTypeDocument(),
            'number_id' => $this->getNumberId(),
            'birthdate' => $this->getBirthDate(),
            'client' => $this->getClient()
        ];
        return $dataArray;
    }
}