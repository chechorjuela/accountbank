import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from "./User/UserList.Component";
import {ProfileComponent} from "./Profile/Profile.Component";
import {AuthGuard} from "../../Infrastructure/Helpers/utils/AuthGuard";

export const AuthRoutes: Routes = [
  {path: 'auth/user', component: UserListComponent, canActivate: [AuthGuard]},
  {path: 'auth/profile', component: ProfileComponent, canActivate: [AuthGuard]},
];
