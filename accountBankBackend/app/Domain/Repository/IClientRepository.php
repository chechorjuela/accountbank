<?php


namespace App\Domain\Repository;


use App\Domain\Model\Client;

interface IClientRepository
{
    public function getAllClient(): ?array;

    public function getByClientId(int $id);

    public function save(Client $client): Client;

    public function delete($id): bool;

    public function findClientByNit(int $nit):? Client;

}