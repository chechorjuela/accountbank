import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {AuthModel} from '../Contracts/Common/AuthModel';

@Injectable()
export class AuthComunicationService {
  private currentUserAnnounce = new Subject<AuthModel>();
  currentUserAnnounce$ = this.currentUserAnnounce.asObservable();
  announceCurrentUSerAuth(auth: AuthModel) {
    this.currentUserAnnounce.next(auth);
  }
}
