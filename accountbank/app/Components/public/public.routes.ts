import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from "./root/app.component";
import {SigninComponent} from "./root/Signin/signin.component";
import {SignupComponent} from "./root/Signup/signup.component";

export const PublicRoutes: Routes = [
  {path: 'home', component: AppComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'signin', component: SigninComponent}
];
