import {ModelSalario} from '../../Infrastructure/Contracts/Common/ModelSalario';

export class EnumSalario {
  public enumSalario: Array<ModelSalario>;
  public arraySalario = ['0 - 800.000', '800.000 - 1.000.000', '1.000.000-4.000.000', '4.000.000 - Mas'];

  constructor() {
    this.enumSalario = Array<ModelSalario>();
    this.arraySalario.forEach((item, index) => {
      let newSalario: ModelSalario = new ModelSalario();
      newSalario.name = item;
      newSalario.value = index;
      this.enumSalario.push(newSalario);
    });
  }
}
