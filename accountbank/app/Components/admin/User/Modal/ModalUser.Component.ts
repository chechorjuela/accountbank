import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MzBaseModal, MzModalComponent, MzToastService} from 'ngx-materialize';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {UserContract} from '../../../../Infrastructure/Contracts/user.contract';
import {CustomValidationService} from '../../../../Infrastructure/Custom/customvalidator.service';
import {ClientContract} from '../../../../Infrastructure/Contracts/client.contract';
import {ClientService} from '../../../../Infrastructure/Services/Client/client.service';
import {ClientComuncationService} from "../../../../Infrastructure/Custom/ClientComuncationService";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-modaluser-selector',
  providers: [CustomValidationService, ClientService],
  styleUrls: ['../../../../assets/public/clientList.css'],
  templateUrl: '../../../../views/admin/user/modal/usermodal.component.html'
})

export class ModalUserComponent extends MzBaseModal implements OnInit, OnDestroy {

  public titleModal = 'Add Client';
  userForm: FormGroup;
  clientContract: ClientContract = new ClientContract();
  clientContractInput: ClientContract = new ClientContract();
  subscriptionClientSelect: Subscription;
  loading = false;
  existNit = false;
  // @ts-ignore
  @ViewChild('modalClient') modalClient: MzModalComponent;
  @Input() userSend: UserContract;
  errorMessageResources = {
    firstname: {
      required: 'Firstname is required'
    },
    lastname: {
      required: 'Lastname is required'
    },
    client: {
      required: 'true'
    },
    date_birthday: {
      required: 'birthDate is required',
      ageAdult: 'Year is menor'
    }
  };

  modalOptions: Materialize.ModalOptions = {
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '15%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.
    },
    complete: () => console.info('Closed'), // Callback for Modal close
  };

  public datepickerOptions: Pickadate.DateOptions = {
    clear: 'Clear', // Clear button text
    close: 'Ok',    // Ok button text
    today: 'Today', // Today button text
    closeOnClear: true,
    closeOnSelect: true,
    format: 'yyyy/mm/dd', // Visible date format (defaulted to formatSubmit if provided otherwise 'd mmmm, yyyy')
    formatSubmit: 'yyyy/mm/dd',   // Return value format (used to set/get value)
    onClose: () => console.info('Close has been invoked.'),
    onOpen: () => console.info('Open has been invoked.'),
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 80,    // Creates a dropdown of 10 years to control year,
  };

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.clientContractInput.firstname = '';
    this.clientContractInput.lastname = '';
    this.clientContractInput.birthdate = null;
    this.clientContractInput.client = false;
    this.clientContractInput.number_id = null;
  }

  constructor(
    private formBuilder: FormBuilder,
    private customValidators: CustomValidationService,
    private toastService: MzToastService,
    private clientService: ClientService,
    private clientCommunicationService: ClientComuncationService,
  ) {
    super();
    this.subscriptionClientSelect = this.clientCommunicationService.sendAnnounceClientEdit$.subscribe(
      client => {
        //this.taskContractInput = task;
      }
    );
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuilder.group({
      firstname: [this.clientContractInput.firstname, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      lastname: [this.clientContractInput.lastname, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      type_document: [this.clientContractInput, Validators.compose([])],
      number_id: [this.clientContractInput.number_id, Validators.compose([
        Validators.required,
        Validators.minLength(10),
      ])],
      client: [this.clientContractInput.client, Validators.compose([])],
      birthdate: [this.clientContractInput.birthdate, Validators.compose([
        Validators.required,
        CustomValidationService.ageAdult
      ])]
    });
  }


  saveUser(event) {
    if (this.userForm.valid) {
      if (typeof this.userForm.value.client == 'undefined' || this.userForm.value.client == null) {
        this.userForm.value.client = false;
      }
      if (!this.existNit) {

        this.clientService.saveItemsClient(this.userForm.value).then(resp => {
          if (resp.Header.status === 'success') {
            this.clientCommunicationService.annouceClientPush(resp.Data);
            this.modalClient.closeModal();
            this.toastService.show('User saved', 1500, 'green');
          }
        }).catch(error => {
          this.toastService.show('Error in server', 1500, 'red');
        });
      } else {
        this.toastService.show('Exist this number id', 1500, 'red');
      }

    } else {
      const getErrors = this.getErrorsForm(this.userForm.controls);
      this.toastService.show(getErrors, 1500, 'red');
    }
  }

  searchNit(e) {
    this.clientService.findClientByNit(e.target.value).then(resp => {
      if (resp.Header.status == 'success') {
        if (resp.Data != null) {
          this.toastService.show('Exist this number id', 1500, 'red');
          this.existNit = true;
        } else {
          this.existNit = false;
        }
      }
    }).catch(error => {

    })

  }

  getErrorsForm(controls) {
    let errorString = '';
    Object.keys(controls).forEach(key => {
      const controlError: ValidationErrors = this.userForm.get(key).errors;
      if (controlError != null) {
        Object.keys(controlError).forEach(keyError => {
          const keyToString = key.toString().toLowerCase();
          errorString += keyToString + (keyError === 'invalidAge' ? ' age not is 18+' : ' is invalid </br>');
        });
      }
    });

    return errorString;
  }
}
