import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {AppSettings} from "../../../config/app.settings";

export interface ILocalStorageUtilsService<T> {
  getItem(key: string): any;

  setItem(key: string, data: any): void;

  getObject(key: string): T;

  watchChanges(): Observable<T>;
}

@Injectable()
export class LocalStorageUtilsService<T> implements ILocalStorageUtilsService<T> {

  private storage = window.localStorage;
  private userInformation: any;
  private lastChanges: any;

  constructor(private appSettings: AppSettings) {

  }

  getItem(key: string): any {
  }

  getObject(key: string): T {
    let info: T;
    try {
      info = JSON.parse(this.storage.getItem(key));
      return info;
    } catch (ex) {
      console.log(ex);
    }
  }

  setItem(key: string, data: any): void {
    this.storage.setItem(key, JSON.stringify(data));
  }

  /**
   *
   */

  /*  watchChanges(): Observable<T> {
  /!*    let changes = new Observable<T>((observer:any) => {
        setInterval(() => {
          if (this.readChanges()) {
            observer.next(this.userInformation);
          }
        }, 1000);
      });
      return changes;*!/
    }*/
  readChanges() {
    let d = this.userInformation;
    if (d != this.lastChanges) {
      this.lastChanges = d;
      return true;
    }

    return false;
  }

  watchChanges(): Observable<T> {
    return undefined;
  }

  deleteAuth() {
    if(this.getObject('UserInfoStorage')){
      this.storage.removeItem('UserInfoStorage');
      this.storage.clear();
    }

  }
}
