export class ResponseapiContract<T> {
  status: number;
  Header: ReponseApiHeaderModel;
  Data: T;
}

export class ReponseApiHeaderModel {
  status: string;
  message: string;
}
