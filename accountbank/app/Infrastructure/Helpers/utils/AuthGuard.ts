import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {LocalStorageUtilsService} from "./LocalStorageUtils.service";
import {Observable} from "rxjs";
import {AuthModel} from "../../Contracts/Common/AuthModel";
import {AuthComunicationService} from "../../Custom/AuthComunication.service";
import {IAuthRepository} from "../../../Repository/Interfaces/Auth/auth.repository";

@Injectable()
export class AuthGuard implements CanActivate {

  private authModel: AuthModel;
  private authComunicationService: AuthComunicationService;

  constructor(private router: Router,
              private localStoreUtils: LocalStorageUtilsService<AuthModel>,
              private loginRepository: IAuthRepository,
  ) {


  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.localStoreUtils.getObject('UserInfoStorage') == null) {
      return this.router.navigate(['/auth/signin'])
    }
    return this.checkAccess(route);
  }

  checkAccess(route: ActivatedRouteSnapshot): Promise<boolean> {
    this.authModel = this.localStoreUtils.getObject('UserInfoStorage');
    if (this.authModel) {
      this.loginRepository.GetUSer().then(resp => {
        if (resp.status == 503 || resp.Header.status == "fail") {
          this.localStoreUtils.deleteAuth();
          this.authComunicationService.announceCurrentUSerAuth(null);
          this.router.navigate(['/auth/signin'])
        }
      })
    }
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
}
