<?php


namespace App\Infrastructure\Repository;


use App\Domain\Model\Client;
use App\Domain\Repository\IClientRepository;

class ClientRepository implements IClientRepository
{

    public function getAllClient(): ?array
    {
        return Client::paginate(10)->toArray();
    }

    public function getByClientId($id)
    {
        return Client::find($id);
    }

    public function save(Client $client): Client
    {
        // TODO: Implement save() method.
        $clientNew = Client::create($client->serializeData());
        return $clientNew;
    }


    public function delete($id): bool
    {
        $client = Client::find($id);
        if ($client->delete()) {
            return true;
        }
        return false;
    }

    public function findClientByNit(int $nit): ?Client
    {
        return Client::where('number_id',$nit)->first();
    }
}