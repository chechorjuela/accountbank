import {Injectable} from '@angular/core';
import {ResponseapiContract} from '../../Infrastructure/Contracts/Common/responseapi.contract';
import {ResponseData} from '../../Infrastructure/Contracts/Common/responseapidata.contract';
import {ClientContract} from '../../Infrastructure/Contracts/client.contract';
import {AppSettings} from '../../config/app.settings';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthModel} from '../../Infrastructure/Contracts/Common/AuthModel';
import {LocalStorageUtilsService} from '../../Infrastructure/Helpers/utils/LocalStorageUtils.service';

@Injectable()
export abstract class IClientRepository {
  abstract getAllClientsPaginate(page: number): Promise<ResponseapiContract<ResponseData<ClientContract>>>;

  abstract saveItemsClient(client: ClientContract): Promise<ResponseapiContract<ClientContract>>;

  abstract deleteItemClient(clientId: number): Promise<ResponseapiContract<ClientContract>>;

  abstract updateItemClient(cliient: ClientContract, id: number): Promise<ResponseapiContract<ClientContract>>;

  abstract findClientByNit(nit: number): Promise<ResponseapiContract<ClientContract>>;
}

@Injectable()
export class ClientRepository implements IClientRepository {
  protected appSettings = new AppSettings();
  private clientUrl = this.appSettings.urls.urlClient;
  private token: AuthModel;
  private baseUrl;
  public headersBasic = new HttpHeaders({'Content-Type': 'application/json', Authorization: null});

  constructor(
    private http: HttpClient,
    private localStorageUtils: LocalStorageUtilsService<AuthModel>
  ) {
    this.baseUrl = this.appSettings.urlApi;
    this.token = this.localStorageUtils.getObject(this.appSettings.userKeyStore);
  }

  deleteItemClient(clientId: number): Promise<ResponseapiContract<ClientContract>> {
    return undefined;
  }

  getAllClientsPaginate(page: number): Promise<ResponseapiContract<ResponseData<ClientContract>>> {
    this.headersBasic.set('Authorization', 'bearer ' + this.token.token);
    const headersoptions = new HttpHeaders().set('Authorization', 'bearer ' + this.token.token);
    // @ts-ignore
    return this.http.get<ResponseapiContract<ResponseData>>(this.baseUrl + this.clientUrl + '?page=' + page, {headers: headersoptions})
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(error => {
      });
  }

  saveItemsClient(client: ClientContract): Promise<ResponseapiContract<ClientContract>> {
    this.headersBasic.set('Authorization', 'bearer ' + this.token.token);
    const headersoptions = new HttpHeaders().set('Authorization', 'bearer ' + this.token.token);
    // @ts-ignore
    return this.http.post<ResponseapiContract<ClientContract>>(this.baseUrl + this.clientUrl, client,{headers: headersoptions})
      .toPromise()
      .then(resp => {
        return resp;
      });
  }

  updateItemClient(cliient: ClientContract, id: number): Promise<ResponseapiContract<ClientContract>> {
    return undefined;
  }

  findClientByNit(nit: number): Promise<ResponseapiContract<ClientContract>> {
    this.headersBasic.set('Authorization', 'bearer ' + this.token.token);
    const headersoptions = new HttpHeaders().set('Authorization', 'bearer ' + this.token.token);
    // @ts-ignore
    return this.http.get<ResponseapiContract<ClientContract>>(this.baseUrl + this.clientUrl + '/findClientByNit?nit=' + nit, {headers: headersoptions})
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(error => {
      });
  }
}
