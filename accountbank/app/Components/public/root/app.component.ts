import { Component } from '@angular/core';
import { MzNavbarModule } from 'ngx-materialize'

@Component({
  selector: 'app-root',
  templateUrl: '../../../views/public/root/app.component.html',
  styleUrls: ['../../../assets/main/app.component.sass']
})
export class AppComponent {
  title = 'accountbank';
}
