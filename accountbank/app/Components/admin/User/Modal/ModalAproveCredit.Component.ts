import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MzBaseModal, MzModalComponent} from 'ngx-materialize';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {CustomValidationService} from '../../../../Infrastructure/Custom/customvalidator.service';
import {ApprovecreditContract} from '../../../../Infrastructure/Contracts/approvecredit.contract';
import {ClientContract} from '../../../../Infrastructure/Contracts/client.contract';
import {EnumSalario} from '../../../../config/Enum/EnumSalario';
import {EnumCreditApproved} from '../../../../config/Enum/EnumCreditApproved';


@Component({
  providers: [CustomValidationService],
  selector: 'app-selector-modalapproveclient',
  templateUrl: '../../../../views/admin/user/modal/clientAproveCredit.component.html',
  styleUrls: ['../../../../assets/public/clientList.css'],
})


export class ModalAproveCreditComponent extends MzBaseModal implements OnInit, OnDestroy {

  public selectOptionSalario;
  private enumSalario: EnumSalario;
  private enumCreditApproved: EnumCreditApproved = new EnumCreditApproved();
  public loading = false;
  public show_message_user = false;
  approveCredictForm: FormGroup;
  public ClientSelected: ClientContract = new ClientContract();
  public clientAproveContract: ApprovecreditContract = new ApprovecreditContract();
  public message_form: string;

  // @ts-ignore
  @ViewChild('modalClientApprove') modalClientApprove: MzModalComponent;

  @Input() ClientSelectedForm: ClientContract;
  constructor(
    private formBuilder: FormBuilder,
    private customValidators: CustomValidationService,
  ) {
    super();
    this.enumSalario = new EnumSalario();
    this.selectOptionSalario = this.enumSalario.enumSalario;
    this.approveCredictForm = this.formBuilder.group({
      company: [this.clientAproveContract.company, Validators.compose([
          Validators.required
        ]
      )],
      nit: [this.clientAproveContract.nit, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
        Validators.pattern('^[0-9]*$'),
      ])],
      value: [this.clientAproveContract.value, Validators.compose([
        Validators.required,
        CustomValidationService.validatorRangeSalario
      ])],
      date_contract: [this.clientAproveContract.date_contract, Validators.compose([
        Validators.required,
        CustomValidationService.validtorYearCompany
      ])]
    });
  }


  public datepickerOptions: Pickadate.DateOptions = {
    clear: 'Clear', // Clear button text
    close: 'Ok',    // Ok button text
    today: 'Today', // Today button text
    closeOnClear: true,
    closeOnSelect: true,
    max: Date.now(),
    format: 'yyyy/mm/dd', // Visible date format (defaulted to formatSubmit if provided otherwise 'd mmmm, yyyy')
    formatSubmit: 'yyyy/mm/dd',   // Return value format (used to set/get value)
    onClose: () => console.info('Close has been invoked.'),
    onOpen: () => console.info('Open has been invoked.'),
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 80,    // Creates a dropdown of 10 years to control year,
  };

  modalOptions: Materialize.ModalOptions = {
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '15%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.
    },
    complete: () => console.info('Closed'), // Callback for Modal close
  };

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.clientAproveContract.nit = null;
    this.clientAproveContract.value = null;
    this.clientAproveContract.date_contract = null;
    this.clientAproveContract.company = '';
  }

  calculeCredit(event) {
    this.loading = false;
    this.message_form = '';
    if (this.approveCredictForm.valid) {
      this.show_message_user = true;
      this.message_form = 'meets the requirements requested by the system, he can have a quota of ' +
        this.enumCreditApproved.arrayCreditApproved[this.approveCredictForm.value.value].creditApproved;
    } else {
      this.show_message_user = true;
      const getErrors = this.getErrorsForm(this.approveCredictForm.controls);
      this.message_form = getErrors;
    }
  }

  getErrorsForm(controls) {
    let errorString = '';
    Object.keys(controls).forEach(key => {
      const controlError: ValidationErrors = this.approveCredictForm.get(key).errors;
      if (controlError != null) {
        Object.keys(controlError).forEach(keyError => {
          const keyToString = key.toString().toLowerCase().replace('_', ' ');
          errorString += keyToString + (keyError === 'invalidAgeCompany' ? ' is invalid' : ' is invalid </br>');
        });
      }
    });
    this.loading = false;
    return errorString;
  }
}
