export class ApprovecreditContract {
  public company: string;
  public nit: number;
  public value: number;
  public date_contract: Date;
}
