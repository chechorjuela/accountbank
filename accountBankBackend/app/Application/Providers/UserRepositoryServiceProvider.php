<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UserRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Domain\Repository\IUserRepositoryInterface',
            'App\Infrastructure\Repository\UserRepository'
        );

        $this->app->bind(
            'App\Domain\Repository\IClientRepository',
            'App\Infrastructure\Repository\ClientRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
