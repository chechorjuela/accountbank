<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 2/03/19
 * Time: 06:51 PM
 */

namespace App\Domain\Repository;


use App\Domain\Model\User;
/*
 *
 */
interface IUserRepositoryInterface
{
    public function getAllUser():?array;
    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id): ?User;

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user):User;

    /**
     * @param string $email
     * @param string $passowrd
     * @return User|null
     */
    public function login(string $email,string $passowrd):?User;
}
