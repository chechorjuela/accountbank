export class AppSettings {
  public urlApi = 'http://127.0.0.1:8000/api/';
  public urls = {
    signInUrl: 'auth/signin',
    signUpUrl: 'auth/signup',
    getUser: 'auth/profile',
    urlClient: 'client',
  };
  public statusResponse = {
    ok: 'success',
    fail: 'failed'
  };

  public userKeyStore = 'UserInfoStorage';
}
