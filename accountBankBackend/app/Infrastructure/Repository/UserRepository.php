<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 2/03/19
 * Time: 07:14 PM
 */

namespace App\Infrastructure\Repository;


use App\Domain\Model\User;
use App\Domain\Repository\IuserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class UserRepository implements IuserRepositoryInterface
{

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id): ?User
    {
        $user = User::find($id);
        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user): User
    {
        $userNew = User::create($user->dataSerialize());
        return $userNew;
    }

    /**
     * @param string $username
     * @param string $passowrd
     * @return User|null
     */
    public function login(string $username, string $passowrd): ?User
    {
        $user = User::where(["username"=>$username])->first();
        if($user){
            if(Hash::check($passowrd,$user->password)){
                $user['token']= auth()->login($user);
                return $user;
            }
        }
        return null;

    }

    public function getAllUser(): ?array
    {
        //$user = JWTAuth::parseToken()->authenticate();
        return $users= User::where('id','!=',1)->paginate(50)->toArray();

    }
}
