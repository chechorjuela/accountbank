<?php


namespace App\Infrastructure\Http\Controllers\Api;


use App\Domain\Model\Client;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Requests\ClientRequest;
use App\Infrastructure\Service\ClientService;
use App\Infrastructure\Service\ResponseApi;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $responseApi;

    private $clientService;

    public function __construct(ResponseApi $_responseApi,ClientService $clientService)
    {
        $this->middleware('jwtAuthenticator');
        $this->responseApi = $_responseApi;
        $this->clientService = $clientService;
    }

    public function index(Request $request){
        return $this->responseApi->returnResponse($this->clientService->getAllUsers(),201);
    }

    public function show(Request $request){

        if($request->get('nit')){

            return $this->responseApi->returnResponse($this->clientService->getFindClientByNit($request->get('nit')),201);

        }
    }
    public function store(ClientRequest $request){
       try{
        if($request->validated()){
            $client = new Client();
            $client->setClient($request->get('client'));
            $client->setFirstname($request->get('firstname'));
            $client->setLastname($request->get('lastname'));
            $client->setBirthDate($request->get('birthdate'));
            $client->setTypeDocument($request->get('type_document'));
            $client->setNumberId($request->get('number_id'));
            return $this->responseApi->returnResponse($this->clientService->createClient($client),201);
        }
       }catch (\Exception $e){
           return $this->responseApi->returnResponse(null,403,$e->getMessage());
       }
    }
}