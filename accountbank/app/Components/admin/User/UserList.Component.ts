import {Component, OnDestroy, OnInit} from '@angular/core';
import {MzBaseModal, MzModalComponent, MzModalService, MzToastService} from 'ngx-materialize';
import {UserContract} from '../../../Infrastructure/Contracts/user.contract';
import {ModalUserComponent} from './Modal/ModalUser.Component';
import {ClientService} from '../../../Infrastructure/Services/Client/client.service';
import {ClientContract} from '../../../Infrastructure/Contracts/client.contract';
import {ModalAproveCreditComponent} from './Modal/ModalAproveCredit.Component';
import {ClientComuncationService} from "../../../Infrastructure/Custom/ClientComuncationService";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-userlist-selector',
  providers: [ClientService],
  styleUrls: ['../../../assets/public/clientList.css'],
  templateUrl: '../../../views/admin/user/userList.component.html'
})

export class UserListComponent implements OnInit, OnDestroy {

  public itemsPerPage: number;
  public totalItems: number;
  public currentPage = 0;
  userSelected: UserContract = new UserContract();
  public clientList = Array<ClientContract>();
  public subscriptionClientPush: Subscription;
  public loadingClient = true;
  ngOnDestroy(): void {
    this.subscriptionClientPush.unsubscribe();
  }

  ngOnInit(): void {
    this.loadingPage();
  }

  constructor(
    private modalService: MzModalService,
    private clientService: ClientService,
    private clientCommunicationService: ClientComuncationService
  ) {

    this.subscriptionClientPush = clientCommunicationService.sendAnnounceClientPush$.subscribe(
      clientPush => {

        this.clientList.push(clientPush);
        this.loadingPage();
      }
    );
  }

  loadingPage() {
    this.clientService.ClientAllPaginate(this.currentPage).then(resp => {
      this.loadingClient = false;
      if (resp.Header.status == 'success') {
        this.clientList = resp.Data.data;
        this.currentPage = resp.Data.current_page;
        this.itemsPerPage = resp.Data.per_page;
        this.totalItems = resp.Data.total;
      }
    }).catch(error => {

    });
  }

  openModalAssament(event, user) {
    this.modalService.open(ModalAproveCreditComponent, {ClientSelectedForm: user});
  }

  openModal(event, user) {
    event.preventDefault();
    if (user != null) {
      this.userSelected = user;
    } else {
      this.userSelected = null;
    }
    // this.taskEmmiter.emit(taskSelect);
    // this.taskCommunicationService.announceTaskEdit(this.userSelected);
    // {taskContract: this.taskSelected}
    this.modalService.open(ModalUserComponent, {userToSend: this.userSelected});
  }

  changePage(e) {
    switch (e.target.innerText) {
      case 'chevron_right':
        if ((this.totalItems / this.itemsPerPage) > this.currentPage) {
          this.currentPage = this.currentPage + 1;
          this.loadingPage();
        }
        break;
      case 'chevron_left':
        if (this.currentPage > 1) {
          this.currentPage = this.currentPage - 1;
          this.loadingPage();
        }
        break;
      case 'first_page':
        if (this.currentPage != 1) {
          this.currentPage = 1;
          this.loadingPage();
        }
        break;
      case 'last_page':
        if ((this.totalItems / this.itemsPerPage) != this.currentPage) {
          const page = this.totalItems / this.itemsPerPage > 1 ? (this.totalItems % this.itemsPerPage) +
            1 : this.totalItems % this.itemsPerPage;
          this.currentPage = page;
          this.loadingPage();
        }
        break;
      default:
        if (this.currentPage != e.target.innerText) {
          this.currentPage = e.target.innerText;
          this.loadingPage();
        }
        break;
    }
  }
}
