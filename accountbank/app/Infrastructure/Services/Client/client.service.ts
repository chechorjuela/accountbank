import {ClientContract} from '../../Contracts/client.contract';
import {ResponseapiContract} from '../../Contracts/Common/responseapi.contract';
import {ResponseData} from '../../Contracts/Common/responseapidata.contract';
import {Injectable} from '@angular/core';
import {IClientRepository} from '../../../Repository/Interfaces/client.repository';
import {AppSettings} from '../../../config/app.settings';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export abstract class IClientService {
  abstract ClientAllPaginate(page: number): Promise<ResponseapiContract<ResponseData<ClientContract>>>;

  abstract saveItemsClient(client: ClientContract): Promise<ResponseapiContract<ClientContract>>;

  abstract deleteItemClient(id: number): Promise<ResponseapiContract<ClientContract>>;

  abstract updateItemClient(client: ClientContract, id: number): Promise<ResponseapiContract<ClientContract>>;

  abstract findClientByNit(nit: number): Promise<ResponseapiContract<ClientContract>>;
}

@Injectable()
export class ClientService implements IClientService {
  constructor(
    public ClientRepository: IClientRepository,
    private appSettings: AppSettings
  ) {

  }

  ClientAllPaginate(page: number): Promise<ResponseapiContract<ResponseData<ClientContract>>> {


    return this.ClientRepository.getAllClientsPaginate(page).then(resp => {
      const clientContractRepository = [];
      resp.Data.data.map((client, index) => {
        /*let indexDate = client.date_expiration.indexOf(" ");
        let dateFormat = task.date_expiration.substring(0,indexDate).replace("-","/").replace("-","/");
        task.date_expiration = dateFormat;*/
        clientContractRepository.push(client);
      });
      if (clientContractRepository.length > 0) {
        resp.Data.data = clientContractRepository;
      }
      return resp;
    });
  }

  deleteItemClient(id: number): Promise<ResponseapiContract<ClientContract>> {
    return undefined;
  }

  saveItemsClient(client: ClientContract): Promise<ResponseapiContract<ClientContract>> {
    return this.ClientRepository.saveItemsClient(client).then(resp => {
      return resp;
    }).catch(error => {
      return error;
    });
  }

  updateItemClient(client: ClientContract, id: number): Promise<ResponseapiContract<ClientContract>> {
    return undefined;
  }

  findClientByNit(nit: number): Promise<ResponseapiContract<ClientContract>> {
    return this.ClientRepository.findClientByNit(nit).then(resp => {
      return resp;
    }).catch(error => {
      return error;
    });
  }
}
