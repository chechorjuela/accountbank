import {Injectable} from "@angular/core";
import {AuthModel} from "../../Contracts/Common/AuthModel";
import {ResponseapiContract} from "../../Contracts/Common/responseapi.contract";
import {SigninContract} from "../../Contracts/signin.contract";
import {UserSignUpContract} from "../../Contracts/signup.contract";
import {IAuthRepository} from "../../../Repository/Interfaces/Auth/auth.repository";

@Injectable()
export abstract class IAuthService {
  abstract Signin(signin: SigninContract): Promise<ResponseapiContract<AuthModel>>;

  abstract Signup(signup: UserSignUpContract): Promise<ResponseapiContract<AuthModel>>;

  abstract Logout();
}

@Injectable()
export class AuthService implements IAuthService {
  constructor(public authRepository: IAuthRepository) {

  }

  Logout() {
  }

  Signin(signin: SigninContract): Promise<ResponseapiContract<AuthModel>> {
    return this.authRepository.SignIn(signin).then(data => {
      return data;
    });

  }

  Signup(signup: UserSignUpContract): Promise<ResponseapiContract<AuthModel>> {
    return this.authRepository.SignUp(signup).then(data => {
      return data;
    });
  }


}
