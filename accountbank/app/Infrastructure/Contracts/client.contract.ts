export class ClientContract {
  public Id: number;
  public firstname: string;
  public lastname: string;
  // tslint:disable-next-line:variable-name
  public number_id: number;
  // tslint:disable-next-line:variable-name
  public type_document: number;
  public birthdate: Date;
  public client: boolean;
}
