import {ClientContract} from '../Contracts/client.contract';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class ClientComuncationService {
  private sendAnnounceClientEdit = new Subject<ClientContract>();
  sendAnnounceClientEdit$ = this.sendAnnounceClientEdit.asObservable();


  announceClientEdit(client: ClientContract) {
    this.sendAnnounceClientEdit.next(client);
  }


  private sendAnnounceClientPush = new Subject<ClientContract>();
  sendAnnounceClientPush$ = this.sendAnnounceClientPush.asObservable();

  annouceClientPush(client: ClientContract) {
    this.sendAnnounceClientPush.next(client);
  }
}
