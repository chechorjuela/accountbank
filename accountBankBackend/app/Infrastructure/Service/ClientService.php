<?php


namespace App\Infrastructure\Service;


use App\Domain\Model\Client;
use App\Domain\Repository\IClientRepository;

class ClientService
{
    private $IClienteRepository;

    public function __construct(IClientRepository $clientRepository)
    {
        $this->IClienteRepository = $clientRepository;
    }

    public function getAllUsers():?array {
        return $this->IClienteRepository->getAllClient();
    }

    public function findClientById($id){
        return $this->IClienteRepository->getByClientId($id);
    }

    public function createClient(Client $client):Client{
        return $this->IClienteRepository->save($client);
    }

    public function updateClient($id,$data){
        return $this->IClienteRepository->update($id,$data);
    }

    public function getFindClientByNit($nit){
        return $this->IClienteRepository->findClientByNit($nit);
    }
}